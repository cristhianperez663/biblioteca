/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     23/11/2022 0:23:58                           */
/*==============================================================*/


drop index BIBLIOTECARIOS_PK;

drop table BIBLIOTECARIOS;

drop index RELATIONSHIP_8_FK;

drop index BODEGA_PK;

drop table BODEGA;

drop index CLIENTE_PK;

drop table CLIENTE;

drop index RELATIONSHIP_4_FK;

drop index DEVOLUCION_PK;

drop table DEVOLUCION;

drop index RELATIONSHIP_1_FK;

drop index EJEMPLARES_PK;

drop table EJEMPLARES;

drop index LIBROS_PK;

drop table LIBROS;

drop index RELATIONSHIP_9_FK;

drop index RELATIONSHIP_3_FK;

drop index RELATIONSHIP_2_FK;

drop index PRESTAMOS_PK;

drop table PRESTAMOS;

drop index RELATIONSHIP_7_FK;

drop index PROVEEDORES_PK;

drop table PROVEEDORES;

drop index RELATIONSHIP_10_FK;

drop index REGISTRO_BIBLIOTICARIOS_PK;

drop table REGISTRO_BIBLIOTICARIOS;

drop index RELATIONSHIP_6_FK;

drop index RELATIONSHIP_5_FK;

drop index REGISTRO_DE_VISITAS_PK;

drop table REGISTRO_DE_VISITAS;

drop index VISITAS_PK;

drop table VISITAS;

/*==============================================================*/
/* Table: BIBLIOTECARIOS                                        */
/*==============================================================*/
create table BIBLIOTECARIOS (
   ID_BIBLIOTECARIO     INT4                 not null,
   NOMBRE               VARCHAR(15)          null,
   APELLIDOS            VARCHAR(30)          null,
   D_N_I_               VARCHAR(10)          null,
   DOMICILIO            VARCHAR(30)          null,
   FECHA_DE_NACIMIENTO  DATE                 null,
   FECHA_DE_ENTRADA     DATE                 null,
   constraint PK_BIBLIOTECARIOS primary key (ID_BIBLIOTECARIO)
);

/*==============================================================*/
/* Index: BIBLIOTECARIOS_PK                                     */
/*==============================================================*/
create unique index BIBLIOTECARIOS_PK on BIBLIOTECARIOS (
ID_BIBLIOTECARIO
);

/*==============================================================*/
/* Table: BODEGA                                                */
/*==============================================================*/
create table BODEGA (
   ID_BODEGA            INT4                 not null,
   ID_LIBRO             INT4                 null,
   ESTADO_LIBRO         VARCHAR(20)          null,
   MOTIVO               VARCHAR(50)          null,
   DESCRIPCION_LIBRO    VARCHAR(200)         null,
   FECHA_DE_BAJA        DATE                 null,
   constraint PK_BODEGA primary key (ID_BODEGA)
);

/*==============================================================*/
/* Index: BODEGA_PK                                             */
/*==============================================================*/
create unique index BODEGA_PK on BODEGA (
ID_BODEGA
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on BODEGA (
ID_LIBRO
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   ID_CLIENTE           INT4                 not null,
   NOMBRE               VARCHAR(15)          null,
   APELLIDOS            VARCHAR(30)          null,
   D_N_I_               VARCHAR(10)          null,
   DOMICILIO            VARCHAR(30)          null,
   PROVINCIA            VARCHAR(20)          null,
   FECHA_DE_NACIMIENTO  DATE                 null,
   constraint PK_CLIENTE primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Index: CLIENTE_PK                                            */
/*==============================================================*/
create unique index CLIENTE_PK on CLIENTE (
ID_CLIENTE
);

/*==============================================================*/
/* Table: DEVOLUCION                                            */
/*==============================================================*/
create table DEVOLUCION (
   ID_DEVOLUCION        INT4                 not null,
   ID_PRESTAMO          INT4                 null,
   FECHA_DE_DEVOLUCION  DATE                 null,
   CONDICION_DE_ENTRADA VARCHAR(20)          null,
   DESCRIPCION_LIBRO    VARCHAR(200)         null,
   constraint PK_DEVOLUCION primary key (ID_DEVOLUCION)
);

/*==============================================================*/
/* Index: DEVOLUCION_PK                                         */
/*==============================================================*/
create unique index DEVOLUCION_PK on DEVOLUCION (
ID_DEVOLUCION
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on DEVOLUCION (
ID_PRESTAMO
);

/*==============================================================*/
/* Table: EJEMPLARES                                            */
/*==============================================================*/
create table EJEMPLARES (
   ID_EJEMPLAR          INT4                 not null,
   ID_LIBRO             INT4                 null,
   CODIGO_DEL_EJEMPLAR  NUMERIC(20)          null,
   NUMERO_DE_EJEMPLARES NUMERIC(200)         null,
   constraint PK_EJEMPLARES primary key (ID_EJEMPLAR)
);

/*==============================================================*/
/* Index: EJEMPLARES_PK                                         */
/*==============================================================*/
create unique index EJEMPLARES_PK on EJEMPLARES (
ID_EJEMPLAR
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_1_FK on EJEMPLARES (
ID_LIBRO
);

/*==============================================================*/
/* Table: LIBROS                                                */
/*==============================================================*/
create table LIBROS (
   ID_LIBRO             INT4                 not null,
   NOMBRE_DEL_LIBRO     VARCHAR(60)          null,
   EDITORIAL            VARCHAR(25)          null,
   AUTOR                VARCHAR(25)          null,
   GENERO               VARCHAR(20)          null,
   PAIS_DEL_AUTOR       VARCHAR(20)          null,
   NUMERO_DE_PAGINAS    NUMERIC(1000)        null,
   ANIOEDICION          DATE                 null,
   PRECIO_DEL_LIBRO     MONEY                null,
   VERSION_DEL_LIBRO    VARCHAR(50)          null,
   ISBN                 VARCHAR(20)          null,
   FECHA_DE_ENTRADA     DATE                 null,
   FECHA_DE_BAJA        DATE                 null,
   NUMERO_DE_PRESTAMOS  INT4                 null,
   COSTO_POR_PRESTAMO   MONEY                null,
   constraint PK_LIBROS primary key (ID_LIBRO)
);

/*==============================================================*/
/* Index: LIBROS_PK                                             */
/*==============================================================*/
create unique index LIBROS_PK on LIBROS (
ID_LIBRO
);

/*==============================================================*/
/* Table: PRESTAMOS                                             */
/*==============================================================*/
create table PRESTAMOS (
   ID_PRESTAMO          INT4                 not null,
   ID_EJEMPLAR          INT4                 null,
   ID_CLIENTE           INT4                 null,
   ID_BIBLIOTECARIO     INT4                 null,
   FECHA_DE_SALIDA      DATE                 null,
   FECHA_DE_DEVOLUCION  DATE                 null,
   PENALIZACION         NUMERIC(350)         null,
   CONDICION_DE_SALIDA  VARCHAR(20)          null,
   NOMBRE_DEL_LIBRO     VARCHAR(60)          null,
   COSTO_POR_PRESTAMO   NUMERIC(500)         null,
   GANAN_POR_PRESTAMO   NUMERIC(500)         null,
   constraint PK_PRESTAMOS primary key (ID_PRESTAMO)
);

/*==============================================================*/
/* Index: PRESTAMOS_PK                                          */
/*==============================================================*/
create unique index PRESTAMOS_PK on PRESTAMOS (
ID_PRESTAMO
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_2_FK on PRESTAMOS (
ID_EJEMPLAR
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_3_FK on PRESTAMOS (
ID_CLIENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on PRESTAMOS (
ID_BIBLIOTECARIO
);

/*==============================================================*/
/* Table: PROVEEDORES                                           */
/*==============================================================*/
create table PROVEEDORES (
   ID_PROVEEDOR         INT4                 not null,
   ID_LIBRO             INT4                 null,
   NOMBRE_PROVEEDOR     VARCHAR(20)          null,
   ESTADO_LIBRO         VARCHAR(20)          null,
   constraint PK_PROVEEDORES primary key (ID_PROVEEDOR)
);

/*==============================================================*/
/* Index: PROVEEDORES_PK                                        */
/*==============================================================*/
create unique index PROVEEDORES_PK on PROVEEDORES (
ID_PROVEEDOR
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on PROVEEDORES (
ID_LIBRO
);

/*==============================================================*/
/* Table: REGISTRO_BIBLIOTICARIOS                               */
/*==============================================================*/
create table REGISTRO_BIBLIOTICARIOS (
   ID_REGISTRO_BIBLIOTECARIO INT4                 not null,
   ID_BIBLIOTECARIO     INT4                 null,
   CEDULA_BIBLIOTECARIO VARCHAR(10)          null,
   ANIO                 DATE                 null,
   CLIENTES_ATENDIDOS   INT4                 null,
   constraint PK_REGISTRO_BIBLIOTICARIOS primary key (ID_REGISTRO_BIBLIOTECARIO)
);

/*==============================================================*/
/* Index: REGISTRO_BIBLIOTICARIOS_PK                            */
/*==============================================================*/
create unique index REGISTRO_BIBLIOTICARIOS_PK on REGISTRO_BIBLIOTICARIOS (
ID_REGISTRO_BIBLIOTECARIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_10_FK on REGISTRO_BIBLIOTICARIOS (
ID_BIBLIOTECARIO
);

/*==============================================================*/
/* Table: REGISTRO_DE_VISITAS                                   */
/*==============================================================*/
create table REGISTRO_DE_VISITAS (
   ID_REGISTRO          INT4                 not null,
   ID_CLIENTE           INT4                 null,
   ID_VISITA            CHAR(10)             null,
   AUTORES_LEIDOS       VARCHAR(150)         null,
   constraint PK_REGISTRO_DE_VISITAS primary key (ID_REGISTRO)
);

/*==============================================================*/
/* Index: REGISTRO_DE_VISITAS_PK                                */
/*==============================================================*/
create unique index REGISTRO_DE_VISITAS_PK on REGISTRO_DE_VISITAS (
ID_REGISTRO
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on REGISTRO_DE_VISITAS (
ID_CLIENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on REGISTRO_DE_VISITAS (
ID_VISITA
);

/*==============================================================*/
/* Table: VISITAS                                               */
/*==============================================================*/
create table VISITAS (
   ID_VISITA            CHAR(10)             not null,
   HORA_ENTRADA         DATE                 null,
   HORA_SALIDA          DATE                 null,
   LIBROS_LEIDOS_       NUMERIC(50)          null,
   constraint PK_VISITAS primary key (ID_VISITA)
);

/*==============================================================*/
/* Index: VISITAS_PK                                            */
/*==============================================================*/
create unique index VISITAS_PK on VISITAS (
ID_VISITA
);

alter table BODEGA
   add constraint FK_BODEGA_RELATIONS_LIBROS foreign key (ID_LIBRO)
      references LIBROS (ID_LIBRO)
      on delete restrict on update restrict;

alter table DEVOLUCION
   add constraint FK_DEVOLUCI_RELATIONS_PRESTAMO foreign key (ID_PRESTAMO)
      references PRESTAMOS (ID_PRESTAMO)
      on delete restrict on update restrict;

alter table EJEMPLARES
   add constraint FK_EJEMPLAR_RELATIONS_LIBROS foreign key (ID_LIBRO)
      references LIBROS (ID_LIBRO)
      on delete restrict on update restrict;

alter table PRESTAMOS
   add constraint FK_PRESTAMO_RELATIONS_EJEMPLAR foreign key (ID_EJEMPLAR)
      references EJEMPLARES (ID_EJEMPLAR)
      on delete restrict on update restrict;

alter table PRESTAMOS
   add constraint FK_PRESTAMO_RELATIONS_CLIENTE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE)
      on delete restrict on update restrict;

alter table PRESTAMOS
   add constraint FK_PRESTAMO_RELATIONS_BIBLIOTE foreign key (ID_BIBLIOTECARIO)
      references BIBLIOTECARIOS (ID_BIBLIOTECARIO)
      on delete restrict on update restrict;

alter table PROVEEDORES
   add constraint FK_PROVEEDO_RELATIONS_LIBROS foreign key (ID_LIBRO)
      references LIBROS (ID_LIBRO)
      on delete restrict on update restrict;

alter table REGISTRO_BIBLIOTICARIOS
   add constraint FK_REGISTRO_RELATIONS_BIBLIOTE foreign key (ID_BIBLIOTECARIO)
      references BIBLIOTECARIOS (ID_BIBLIOTECARIO)
      on delete restrict on update restrict;

alter table REGISTRO_DE_VISITAS
   add constraint FK_REGISTRO_RELATIONS_CLIENTE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE)
      on delete restrict on update restrict;

alter table REGISTRO_DE_VISITAS
   add constraint FK_REGISTRO_RELATIONS_VISITAS foreign key (ID_VISITA)
      references VISITAS (ID_VISITA)
      on delete restrict on update restrict;

